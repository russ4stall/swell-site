# Swellcoin site

The Swellcoin website is a static site generated using node js tools.

### Required Software
1. Node.js <https://nodejs.org/en/>
1. Git <https://git-scm.com/downloads>

### Getting Started!


1. Clone the repository.
    
    ```
    git clone https://{your-username}@bitbucket.org/russ4stall/swell-site.git
    ```
    
1. Install node dependencies from inside project directory.
    
    ```
    cd swell-site
    ```
    
    ```
    npm install
    ```
    
1. Build the site. (This outputs all build assets in a `dist` directory.)
    
    ```
    npm run build
    ```
    
1. Install [http-server](https://www.npmjs.com/package/http-server) for hosting the site locally.
    
    ```
    npm instal
    ```
    
1. To view the site, start the server and navigate to <http://localhost:8080> in your browser.
    
    ```
    http-server dist
    ```

## Development
When making changes to the html templates or sass files, you must rerun `npm run build` to generate the site

> **Tip:** You can use `npm run sasswatch` to automatically build the css when a sass file is saved.

## Deployment
~~The site is configured using `bitbucket-pipelines.yml` to build and deploy when a push is made to the master branch in Bitbucket.~~
This site now deploys to Netlify. **The build assets must be built locally, then pushed to master branch.** (Netlify's build servers can't build for some reason; possibly node-sass version stuff.)