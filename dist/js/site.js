$(document).ready(function () {
    $.get( ".netlify/functions/get-businesses", function( data ) {
        data.records.forEach(x => {
            // var row = "<tr><td><a href='" + x.url + "'>" + x.businessName + "</a></td>" +
            // "<td><a href='tel:"+ x.businessPhone +"'>"+x.businessPhone+"</a></td>" +
            // "<td style='text-align: center;'>"+ x.giftCards +"</td>" +
            // "<td style='text-align: center;'>"+ x.online +"</td>" +
            // "<td style='text-align: center;'>"+ x.curbside +"</td>" +
            // "<td style='text-align: center;'>"+ x.delivery +"</td>";

            var row = `
                    <tr><td><a href='x.url' target='_blank'>${x.businessName}</a></td>
                    <td><a href='tel:${x.businessPhone}'>${x.businessPhone}</a></td>
                    <td class="social-col" style='text-align: center;'>
                    `;
                    if (x.facebook != undefined) {
                        row = row + 
                        `<a href='${x.facebook}' target='_blank' style='text-decoration: none;'>
                            <i class='fab fa-facebook-square'></i>
                        </a> `;
                    }
                    if (x.instagram != undefined) {
                        row = row + 
                        `<a href='${x.instagram}' target='_blank' style='text-decoration: none;'>
                            <i class='fab fa-instagram-square'></i>
                        </a>`;
                    }
                    row = row + 
                    `
                    <td style='text-align: center;'>${x.giftCards}</td>
                    <td style='text-align: center;'>${x.online}</td>
                    <td style='text-align: center;'>${x.curbside}</td>
                    <td style='text-align: center;'>${x.delivery}</td>
                    `;
            $('#BizTable tbody').append(row);
        });
        $('#BizTable').DataTable();
    });

    //Submit Biz
    $("#AddBizForm").submit(function( event ) {
        event.preventDefault();
        $.post(".netlify/functions/add-business", $("#AddBizForm").serialize());
        window.location.href = "/submit-biz-success";
      });

    // MOBILE NAVBAR TOGGLE
    $("#nav-toggle").click(function () {
        $(this).toggleClass('open');
        $("#main-nav ul").slideToggle();

        // delay change of logo color 
        var $navTitle = $("#nav-title");
        var delayTime = 80;
        if ($navTitle.hasClass("open")) {
            delayTime = 300;
        }

        $($navTitle).delay(delayTime).queue(function (next) {
            $(this).toggleClass('open');
            next();
        });
    });

    // CAROUSEL

    $(".carousel").slick({
        'autoplay': true,
        'autoplaySpeed': 5000,
        'arrows': false
    });

    // CONTACT FORM

    var $contactForm = $("#contact-form");
    $contactForm.find("button").click(function () {

        var formData = $contactForm.serializeArray();

        $.ajax({
            url: "https://formspree.io/contact@swellcoin.com",
            method: "POST",
            data: formData,
            dataType: "json"
        }).done(function () {
            $contactForm.hide();
            $("#success-msg").show();
        }).fail(function () {
            alert("There was an error submitting this form. Please try again later, or simply send us an email at contact@swellcoin.com");
        });
    });

    //FOOTER
    var year = new Date().getFullYear();
    console.log("year: " + year);
    $("#CopyrightYear").html(year);
}); 