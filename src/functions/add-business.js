import querystring from "querystring";
const Airtable = require('airtable')

exports.handler = function(event, context, callback) {

    const body = querystring.parse(event.body);

    var email = body.email;
    var personalPhone = body.personalPhone;
    var bizName = body.businessName;
    var url = body.url;
    var facebook = body.facebook;
    var instagram = body.instagram;
    var bizPhone = body.businessPhone;
    var giftCards = body.giftCards === "on";
    var online = body.online === "on";
    var curbside = body.curbside === "on";
    var delivery = body.delivery === "on";
    
    Airtable.configure({
      endpointUrl: 'https://api.airtable.com',
      apiKey: 'key4NH7XnBQVBUOyh'
    })
  const base = Airtable.base('appuwtEWnaY9pevj6')
 
  base('Tallahassee').create([
    {
      "fields": {
        "Email": email,
        "PersonalPhone": personalPhone,
        "BusinessName": bizName,
        "Url": url,
        "Facebook": facebook,
        "Instagram": instagram,
        "BusinessPhone": bizPhone,
        "GiftCards": giftCards,
        "Online": online,
        "Curbside": curbside,
        "Delivery": delivery
      }
    }
  ], function(err, records) {
    if (err) {
      console.error(err);
      return;
    }
    records.forEach(function (record) {
      console.log(record.getId());
    });
  });

}