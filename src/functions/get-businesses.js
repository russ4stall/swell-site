const Airtable = require('airtable')

exports.handler = function(event, context, callback) {

  Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'key4NH7XnBQVBUOyh'
  })

  const base = Airtable.base('appuwtEWnaY9pevj6')
  const allRecords = []
  base('Tallahassee')
    .select({
      //maxRecords: 100,
      view: 'Grid view'
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function(record) {
          console.log(record.get());
          allRecords.push(
            {
              "businessName": record.get('BusinessName'),
              "businessPhone": record.get('BusinessPhone'),
              "url": record.get('Url'),
              "facebook": record.get('Facebook'),
              "instagram": record.get('Instagram'),
              "giftCards": record.get('GiftCards') ?? false ? "Yes" : "No",
              "online": record.get('Online') ?? false ? "Yes" : "No",
              "curbside": record.get('Curbside') ?? false ? "Yes" : "No",
              "delivery": record.get('Delivery') ?? false ? "Yes" : "No"
            }
          );
        })
        fetchNextPage()
      },
      function done(err) {
        if (err) {
          callback(err)
        } else {
          const body = JSON.stringify({ records: allRecords })
          const response = {
            statusCode: 200,
            body: body,
            headers: {
              'content-type': 'application/json',
              'cache-control': 'Cache-Control: max-age=60, public'
            }
          }
          callback(null, response)
        }
      }
    )
}